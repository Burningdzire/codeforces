#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[])
{
	long n, m;
	cin>>n>>m;
	string str;
	cin>>str;
	int index;
	char ch;

	long i = 0, j = 0, currSegmentLength, segmentLength = 0, segmentCount = 0;
	while(i < n)
	{
		while(i < n && str[i] != '.')
			i++;
		if(i < n && str[i] == '.')
		{
			j = i;
			segmentCount++;
			currSegmentLength = 0;
			while(j < n && str[j] == '.')
			{
				currSegmentLength = max(currSegmentLength, j - i + 1);
				j ++;
			}
			segmentLength += currSegmentLength;
			i = j;
		}
	}

	while(m--)
	{
		cin>>index>>ch;
		index -= 1;
		if(str[index] != ch)
		{

			bool left = false, right = false;
			if(str[index] != '.')
			{
				segmentLength ++;

				if(index == 0)
					left = true;
				else if(str[index - 1] != '.')
					left = true;

				if(index == n-1)
					right = true;
				else if(str[index + 1] != '.')
					right = true;

				if(left == true && right == true)
					segmentCount ++;
				if(left == false && right == false)
					segmentCount --;
			}
			else
			{
				segmentLength --;

				if(index == 0)
					left = true;
				else if(str[index - 1] != '.')
					left = true;

				if(index == n-1)
					right = true;
				else if(str[index + 1] != '.')
					right = true;

				if(left == true && right == true)
					segmentCount --;
				if(left == false && right == false)
					segmentCount ++;
			}
			str[index] = ch;
		}
		cout<<segmentLength- segmentCount<<endl;
	}
	return 0;
}
