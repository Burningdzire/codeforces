#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[])
{
	int r, c;
	string temp;
	cin>>r>>c;
	char loc[r][c];
	for (int i = 0; i < r; ++i)
	{
		cin>>temp;
		for (int j = 0; j < c; ++j)
		{
			loc[i][j] = temp[j];
		}
	}
	int count = 0;
	for (int i = 0; i < r; ++i)
	{
		for (int j = 0; j < c; ++j)
		{
			bool danger = false;
			if(loc[i][j] == 'P')
			{
				if(i > 0 && loc[i-1][j] == 'W')
				{
					danger = true;
					loc[i-1][j] = '.';
				}
				else if(i < r-1 && loc[i+1][j] == 'W')
				{
					danger = true;
					loc[i+1][j] = '.';
				}
				else if(j > 0 && loc[i][j-1] == 'W')
				{
					danger = true;
					loc[i][j-1] = '.';
				}
				else if(j < c-1 && loc[i][j+1] == 'W')
				{
					danger = true;
					loc[i][j+1] = '.';
				}
			}
			if(danger)
				count++;
		}
	}
	cout<<count;
	return 0;
}

