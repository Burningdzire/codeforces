#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	int d, sumTime, i;
	cin>>d>>sumTime;
	int minTime[d] = {}, maxTime[d] = {}, sumMin = 0, sumMax = 0;
	for (i = 0; i < d; ++i)
	{
		cin>>minTime[i]>>maxTime[i];
		sumMin += minTime[i];
		sumMax += maxTime[i];
	}

	if(sumTime < sumMin || sumMax < sumTime)
	{
		cout<<"NO";
	}
	else
	{
		i = 0;
		sumTime -= sumMin;
		while(sumTime > 0)
		{
			if(sumTime >= (maxTime[i] - minTime[i]))
			{
				int temp = (maxTime[i] - minTime[i]);
				minTime[i] += temp;
				sumTime -= temp;
			}
			else
			{
				minTime[i] += sumTime;
				sumTime = 0;
			}
			i++;
		}
		cout<<"YES\n";
		for (i = 0; i < d; ++i)
		{
			cout<<minTime[i]<<" ";
		}
	}
	
	return 0;
}
