#include <iostream>
using namespace std;

int main(int argc, char const *argv[])
{
	long long n, k;
	cin>>n>>k;
	if(k == 1)
	{
		cout<<n;
	}
	else
	{
		long long ans = 1;
		while(ans < n)
		{
			ans = ans*2 + 1;
		}
		cout<<ans;
	}
	return 0;
}
